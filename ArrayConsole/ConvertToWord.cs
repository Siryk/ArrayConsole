﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayConsole
{
    class ConvertToWord
    {
        enum decade
        {
            none = 0,
            twenty = 2,
            thirty = 3,
            forty = 4,
            fifty = 5,
            sixty = 6,
            seventy = 7,
            eighty = 8,
            ninety = 9
        }

        enum ones
        {
            zero = 0,
            one = 1,
            two = 2,
            three = 3,
            four = 4,
            five = 5,
            six = 6,
            seven = 7,
            eight = 8,
            nine = 9,
            ten = 10,
            eleven = 11,
            twelve = 12,
            thirteen = 13,
            fourteen = 14,
            fifteen = 15,
            sixteen = 16,
            seventeen = 17,
            eighteen = 18,
            nineteen = 19
        }


        public string Convert(int digit,int numb)
        {
            string result = "";
            switch (digit)
            {
                case 1:
                    if ((numb).Equals(0))
                    {
                        return "";
                    }
                    else
                    {
                        return(ones)numb + "";
                    }
                    break;
                case 10:
                    if ((numb / 10).Equals(0))
                    {
                        result = "" + Convert(digit / 10, numb % digit);
                    }
                    else
                    {
                        if (numb > 19)
                        {

                            result = (decade) (numb / digit) + " " + Convert(digit / 10, numb % digit);
                        }
                        else
                        {
                            result = (ones) (numb) + "";
                        }

                    }
                    return result;
                    break;
                case 100:
                    if ((numb / 100).Equals(0))
                    {
                        result = "" + Convert(digit / 10, numb % digit);
                    }
                    else
                    {
                        result = (ones)(numb / digit) + " " + "hungred" + " " + Convert(digit / 10, numb % digit);
                    }
                    return result;
                    break;
                case 1000:
                    if (numb.Equals(0))
                    {
                        return (ones)numb + "";
                    }
                    if ((numb / 1000).Equals(0))
                    {
                        result = "" + Convert(digit / 10, numb % digit);
                    }
                    else
                    {
                        result = (ones)(numb / digit) + " " + "thousand" + " " + Convert(digit / 10, numb % digit);
                    }
                    return result;
                    break; 
            }

            return result;
        }

        public string ConvertAfterPoint(int n)
        {
            if (n == 0) return "";
            var result = ConvertAfterPoint(n / 10) + " " + (ones)(n % 10);
            return result;
        }
    }
}
