﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            int size = 0;
            
            Console.WriteLine("Enter size array:");
            
            do
            {

                Int32.TryParse(Console.ReadLine(),out size);
                if (size <= 0)
                {
                    Console.Clear();
                    Console.Write("You entered an incorrect value, please enter a new one: ");
                }
                else
                {
                    break;
                }

            } while (true);

            Console.Clear();

            ArrayConsole<object>.ArrayValue = new object[size];
            var rnd = new ArrayConsole.Randomize();
            rnd.Generate(ArrayConsole<object>.ArrayValue);

            Console.WriteLine("Last operation: (NULL).\n{0}", "------------------------------------");

            do
            {

                Console.WriteLine("Menu:\n" +
                                  " 1) Minumum\n" +
                                  " 2) Maximum\n" +
                                  " 3) Descending sort\n" +
                                  " 4) Ascending sort\n" +
                                  " 5) Show array\n" +
                                  " 6) Convert to double\n" +
                                  " 7) Convert to world\n" +
                                  " 8) Exit");

                Console.Write("Press button:");

                string buf = Console.ReadLine();

                switch (buf)
                {
                    case "1": 
                        Console.Clear();//Код обработки нажатия 1 (пункта 1)
                        Console.WriteLine(ArrayConsole<object>.Min());
                        Console.WriteLine("Press any button for back to menu.....");
                        Console.ReadKey();
                        Console.Clear();
                        Console.WriteLine("Last operation: (MIN).\n{0}", "------------------------------------");
                        break; // выход из case 1 и переход к новому циклу
                    case "2":
                        Console.Clear();//Код обработки нажатия 1 (пункта 1)
                        Console.WriteLine(ArrayConsole<object>.Max());
                        Console.WriteLine("Press any button for back to menu.....");
                        Console.ReadKey();
                        Console.Clear();
                        Console.WriteLine("Last operation: (MAX).\n{0}", "------------------------------------");
                        break;
                    case "3":
                        Console.Clear();
                        ArrayConsole<object>.Desc();
                        Console.WriteLine("Press any button for back to menu.....");
                        Console.ReadKey();
                        Console.Clear();
                        Console.WriteLine("Last operation: (DESC).\n{0}", "------------------------------------");
                        break; // возвращает из ф-ции значение 3
                    case "4":
                        Console.Clear();
                        ArrayConsole<object>.Asc();
                        Console.WriteLine("Press any button for back to menu.....");
                        Console.ReadKey();
                        Console.Clear();
                        Console.WriteLine("Last operation: (ASC).\n{0}", "------------------------------------");
                        break;
                    case "5":
                        Console.Clear();
                        ArrayConsole<object>.Show();
                        Console.WriteLine("Press any button for back to menu.....");
                        Console.ReadKey();
                        Console.Clear();
                        Console.WriteLine("Last operation: (SHOW).\n{0}", "------------------------------------");
                        break;
                    case "6":
                        Console.Clear();
                        rnd.ConvertToDouble(ArrayConsole<object>.ArrayValue);
                        ArrayConsole<object>.Show();
                        Console.WriteLine("Press any button for back to menu.....");
                        Console.ReadKey();
                        Console.Clear();
                        Console.WriteLine("Last operation: (TO_DOUBLE).\n{0}", "------------------------------------");
                        break;
                    case "7":
                        Console.Clear();
                        foreach (var obj in ArrayConsole<object>.ArrayValue)
                        {
                            var localValue = obj.ToString().Split('.', ',');
                            if (localValue.Length > 1)
                            {
                                Console.WriteLine("{0}:{1} point{2}",obj, new ConvertToWord().Convert(1000, Convert.ToInt32(localValue[0])), new ConvertToWord().ConvertAfterPoint(Convert.ToInt32(localValue[1])));
                            }
                            else
                            {
                                Console.WriteLine("{0}:{1}", obj, new ConvertToWord().Convert(1000, (dynamic)obj));
                            }
                        }
                        Console.WriteLine("Press any button for back to menu.....");
                        Console.ReadKey();
                        Console.Clear();
                        Console.WriteLine("Last operation: (TO_WORD).\n{0}", "------------------------------------");
                        break;
                    case "8":
                        return;
                    default:
                        Console.Clear();
                        Console.WriteLine("You press not using keypad.");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                }
            } while (true);   
        }
    }
}
