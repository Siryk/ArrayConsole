﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayConsole
{
    public static class ArrayConsole<T> 
    {
        private static T[] _arrayValue;

        public static T[] ArrayValue
        {
            get { return _arrayValue; }
            set { _arrayValue = value;}
        }

        public static T Min(T [] input)
        {
            return input.Min();
        }

        public static T Min()
        {
            return _arrayValue.Min();
        }

        public static T Max(T [] input)
        {
            return input.Max();
        }

        public static T Max()
        {
            return _arrayValue.Max();
        }

        public static T Sum(T [] input)
        {
            T result = input[0];

            for (int i = 1; i < input.Length; i++)
            {
                result += (dynamic) input[i];
            }

            return result;
        }

        public static T Sum()
        {
            T result = _arrayValue[0];

            for (int i = 1; i < _arrayValue.Length; i++)
            {
                result += (dynamic)_arrayValue[i];
            }

            return result;
        }

        public static void Desc(ref T[] input)
        {
            Array.Sort(input);
            Array.Reverse(input);
            Show(input);
        }

        public static void Desc()
        {
            Array.Sort(_arrayValue);
            Array.Reverse(_arrayValue);
            Show();
        }

        public static void Asc(ref T[] input)
        {
            Array.Sort(input);
            Show(input);
        }

        public static void Asc()
        {
            Array.Sort(_arrayValue);
            Show();
        }

        public static void Show(T[] input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                Console.WriteLine("[{0}]:{1}",i,input[i]);
            }
        }
        public static void Show()
        {
            for (int i = 0; i < _arrayValue.Length; i++)
            {
                Console.WriteLine("[{0}]:{1}", i, _arrayValue[i]);
            }
        }
    }
}
