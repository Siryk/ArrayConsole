﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayConsole
{
    public class Randomize
    {
        private const int minValue = 0;
        private const int maxValue = 1000;

        public void Generate<T>(T[] input)
        {
            var rnd = new Random();
            for (int i = 0; i < input.Length; i++)
            {
                input[i] = (dynamic) rnd.Next(minValue, maxValue);
            }
        }

        public void ConvertToDouble<T>(T[] input)
        {
            var max = Convert.ToDouble(input.Max());
            var min = Convert.ToDouble(input.Min());

            if (min.Equals(0.0))
            {
                min = 1.0;
            }

            for (int i = 0; i < input.Length; i++)
            {
                input[i] = Math.Round(((dynamic) input[i] + max) / min,4);
            }
        }
    }
}
